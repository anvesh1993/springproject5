package com.example.Controller;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication

public class ControllerwithDatabaseApplication {

	public static void main(String[] args) {
		SpringApplication.run(ControllerwithDatabaseApplication.class, args);
	}

}

