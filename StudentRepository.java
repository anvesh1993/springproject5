package com.example.Controller;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
@Repository
public interface StudentRepository extends JpaRepository<Student, Integer>
{
public List<Student>findByAge(int age);
public List<Student>findByIdAndAge(int id,int age);
public List<Student>findByIdOrAge(int id,int age);
public List<Student>findByIdBetween(int id,int id1);
public List<Student>findByIdLessThan(int id);
public List<Student>findByIdGreaterThan(int id);
public List<Student>findByIdLessThanEqual(int id);
public List<Student>findByIdGreaterThanEqual(int id);
public List<Student>findByIdLike(int id);
public List<Student>findByIdNotLike(int id);
public List<Student>findByNameStartingWith(String name);
public List<Student>findAllByOrderByIdDesc();
}
